package com.example.calculator2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.content.Intent;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

public class MainActivity2 extends AppCompatActivity {

    TextView Hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Hasil = (TextView) findViewById(R.id.angkaKeluar);

        String hasil = getIntent().getStringExtra("hasil");

        Hasil.setText(hasil);
    }
}
